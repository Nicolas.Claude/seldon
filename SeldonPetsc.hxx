#ifndef SELDON_FILE_SELDONPETSC_H
#define SELDON_FILE_SELDONPETSC_H


#include "vector/PetscVector.cxx"
#include "matrix/PetscMatrix.cxx"

#include "matrix/FunctionsPetsc.cxx"
#include "computation/basic_functions/Functions_MatrixPetsc.cxx"
#include "computation/basic_functions/Functions_MatVectPetsc.cxx"
#include "computation/basic_functions/Functions_VectorPetsc.cxx"



#endif
