
#ifndef SELDON_FILE_FUNCTIONS_PETSC_HXX

namespace Seldon
{

  template <class T0, class Allocator0, class T1, class Allocator1>
  void GetRow(const Matrix<T0, General, PETScMPIAIJ, Allocator0>& M,
	      int i, Vector<T1, PETScPar, Allocator1>& X);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M,
        int j, Vector<T1, PETScSeq, Allocator1>& X);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M,
        int j, Vector<T1, PETScPar, Allocator1>& X);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScMPIAIJ, Allocator0>& M,
        int j, Vector<T1, PETScPar, Allocator1>& X);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void SetRow(const Vector<T1, PETScSeq, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void SetRow(const Vector<T1, PETScPar, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
            class T1, class Prop1, class Allocator1>
  void SetRow(const Vector<T1, Prop1, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScSeq, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
            class T1, class Prop1, class Allocator1>
  void SetCol(const Vector<T1, Prop1, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScPar, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M);

  template <class T0, class Prop0, class Allocator0,
	    class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScPar, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIAIJ, Allocator0>& M);

}


#define SELDON_FILE_FUNCTIONS_PETSC_HXX
#endif
