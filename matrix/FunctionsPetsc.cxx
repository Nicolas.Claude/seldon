#ifndef SELDON_FILE_FUNCTIONS_PETSC_CXX

#include "FunctionsPetsc.hxx"



namespace Seldon
{


  //! Extracts a row from a PETSc matrix
  /*!
    \param M sparse matrix
    \param i row index
    \param X row extracted
    X = M(i, :)
  */
  template <class T, class Allocator0, class Allocator1>
  void GetRow(const Matrix<T, General, PETScMPIAIJ, Allocator0>& M,
	      int i, Vector<T, PETScPar, Allocator1>& X)
  {
#ifdef SELDON_CHECK_BOUNDS
    size_t m = M.GetM();
    if (i >= m)
      throw WrongIndex("GetRow()",
                       string("Index should be in [0, ") + to_str(m - 1)
                       + "], but is equal to " + to_str(i) + ".");
#endif
    X.Reallocate(M.GetN());
    int a, b;
    M.GetProcessorRowRange(a, b);

    if (i >= a && i < b)
      {
        int  ncols;
        const int * cols;
        const T * vals ;

        MatGetRow(M.GetPetscMatrix(), i, &ncols, &cols,  &vals);

        for (int j = 0; j < ncols ; j++ )
          X.SetBuffer(cols[j], vals[j]);
      }

    X.Flush();
  }

  
  //! Extracts a column from a matrix.
  /*!
    \param[in] M matrix.
    \param[in] j column index.
    \param[out] X extracted column.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M,
              int j, Vector<T1, PETScSeq, Allocator1>& X)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      X.SetBuffer(i, M(i, j));
    X.Flush();
  }


  //! Extracts a column from a matrix.
  /*!
    \param[in] M matrix.
    \param[in] j column index.
    \param[out] X extracted column.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M,
              int j, Vector<T1, PETScPar, Allocator1>& X)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      X.SetBuffer(i, M(i, j));
    X.Flush();
  }


  //! Extracts a column from a matrix.
  /*!
    \param[in] M matrix.
    \param[in] j column index.
    \param[out] X extracted column.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void GetCol(const Matrix<T0, Prop0, PETScMPIAIJ, Allocator0>& M,
              int j, Vector<T1, PETScPar, Allocator1>& X)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      X.SetBuffer(i, M(i, j));
    X.Flush();
  }

  
  //! Sets a row of a matrix.
  /*!
    \param[in] X new row \a i of \a M.
    \param[in] i row index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void SetRow(const Vector<T1, PETScSeq, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M)
  {
    for (int j = 0; j < M.GetN(); j++)
      M.SetBuffer(i, j, X(j));
    M.Flush();
  }


  //! Sets a row of a matrix.
  /*!
    \param[in] X new row \a i of \a M.
    \param[in] i row index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void SetRow(const Vector<T1, PETScPar, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M)
  {
    for (int j = 0; j < M.GetN(); j++)
      M.SetBuffer(i, j, X(j));
    M.Flush();
  }

  
  //! Sets a row of a matrix.
  /*!
    \param[in] X new row \a i of \a M.
    \param[in] i row index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Prop1, class Allocator1>
  void SetRow(const Vector<T1, Prop1, Allocator1>& X,
              int i, Matrix<T0, Prop0, PETScMPIAIJ, Allocator0>& M)
  {
    for (int j = 0; j < M.GetN(); j++)
      M.SetBuffer(i, j, X(j));
    M.Flush();
  }


 //! Sets a column of a matrix.
  /*!
    \param[in] X new column \a j of \a M.
    \param[in] j column index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScSeq, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScSeqDense, Allocator0>& M)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      M.SetBuffer(i, j, X(i));
    M.Flush();
  }


  //! Sets a column of a matrix.
  /*!
    \param[in] X new column \a j of \a M.
    \param[in] j column index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScPar, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      M.SetBuffer(i, j, X(i));
    M.Flush();
  }


  //! Sets a column of a matrix.
  /*!
    \param[in] X new column \a j of \a M.
    \param[in] j column index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Allocator1>
  void SetCol(const Vector<T1, PETScPar, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIAIJ, Allocator0>& M)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      M.SetBuffer(i, j, X(i));
    M.Flush();
  }


  //! Sets a column of a matrix.
  /*!
    \param[in] X new column \a j of \a M.
    \param[in] j column index.
    \param[out] M matrix.
  */
  template <class T0, class Prop0, class Allocator0,
            class T1, class Prop1, class Allocator1>
  void SetCol(const Vector<T1, Prop1, Allocator1>& X,
              int j, Matrix<T0, Prop0, PETScMPIDense, Allocator0>& M)
  {
    int a, b;
    M.GetProcessorRowRange(a, b);
    for (int i = a; i < b; i++)
      M.SetBuffer(i, j, X(i));
    M.Flush();
  }

  
} // namespace Seldon


#define SELDON_FILE_FUNCTIONS_PETSC_CXX
#endif
