#ifndef SELDON_FILE_FUNCTIONS_MATRIX_PETSC_CXX
#define SELDON_FILE_FUNCTIONS_MATRIX_PETSC_CXX


#include "Functions_MatrixPetsc.hxx"


namespace Seldon
{


  /////////
  // MLT //


//! Multiplies a matrix by a scalar.
  /*!
    \param[in] alpha scalar.
    \param[in,out] M matrix to be multiplied.
  */
  template <class T,
	    class Prop1, class Allocator1>
  void Mlt(const T alpha, Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A)
  {
    T alpha_ = alpha;
    MatScale(A.GetPetscMatrix(), alpha_);
  }


  // MLT //
  /////////


  ////////////
  // MLTADD //


//! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$ where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] A matrix.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B, times \a
    alpha, plus \a beta times \a C.
  */
  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Prop4, class Allocator4>
  void MltAdd(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIDense, Allocator1>& A,
              const Matrix<T2, General, RowMajor, Allocator2>& B,
              const T3 beta,
              Matrix<T4, Prop4, PETScMPIDense, Allocator4>& C)
  {
    int na = A.GetN();
    int mc = C.GetM();
    int nc = C.GetN();

#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(A, B, C, "MltAdd(alpha, A, B, beta, C)");
#endif
    T1 *local_a;
    MatGetArray(A.GetPetscMatrix(), &local_a);
    int nlocal_A;
    int mlocal_A;
    MatGetLocalSize(A.GetPetscMatrix(), &mlocal_A, &nlocal_A);
    Matrix<T1, Prop1, ColMajor, Allocator1> local_A;
    local_A.SetData(mlocal_A, na, local_a);

    T4 *local_c;
    MatGetArray(C.GetPetscMatrix(), &local_c);
    int nlocal_C;
    int mlocal_C;
    MatGetLocalSize(C.GetPetscMatrix(), &mlocal_C, &nlocal_C);
    Matrix<T4, Prop4, ColMajor, Allocator4> local_C;
    local_C.SetData(mlocal_C, nc, local_c);

    MltAdd(alpha, local_A, B, beta, local_C);

    local_A.Nullify();
    MatRestoreArray(A.GetPetscMatrix(), &local_a);
    A.Flush();

    local_C.Nullify();
    MatRestoreArray(C.GetPetscMatrix(), &local_c);
    C.Flush();
  }


  //! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$ where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] A matrix.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B, times \a
    alpha, plus \a beta times \a C.
  */
  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Prop4, class Allocator4>
  void MltAdd(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& A,
              const Matrix<T2, General, RowMajor, Allocator2>& B,
              const T3 beta,
              Matrix<T4, Prop4, PETScMPIAIJ, Allocator4>& C)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(A, B, C, "MltAdd(alpha, A, B, beta, C)");
#endif

    int m = B.GetM();
    int n = B.GetN();
    Matrix<T4, Prop4, PETScMPIAIJ, Allocator4> temp(m, n);
    int mmin;
    int mmax;
    MatGetOwnershipRange(C.GetPetscMatrix(), &mmin, &mmax);
    for(unsigned int i = mmin; i < mmax; i++)
      {
        for(unsigned int j = 0; j < n; j++)
          {
            temp.SetBuffer(i,j,B(i,j));
          }
      }
    temp.Flush();
    MltAdd(alpha, A, temp, beta, C);
  }

  //! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$ where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] A matrix.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B, times \a
    alpha, plus \a beta times \a C.
  */
  template <class T,
            class Prop1, class Allocator1,
            class Allocator2,
            class Allocator4>
  void MltAdd(const T alpha,
              const Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A,
              const Matrix<T, General, RowMajor, Allocator2>& B,
              const T beta,
              Matrix<T, General, RowMajor, Allocator4>& C)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(A, B, C, "MltAdd(alpha, A, B, beta, C)");
#endif
    int A_m = A.GetM();
    int A_n = A.GetN();
    Matrix<T, General, RowMajor, Allocator4> temp(A_m, A_n);
    for (unsigned int i = 0; i < A_m; i++)
      {
        for (unsigned int j =  0 ; j < A_n; j++)
          {
            temp(i,j) = A.GetOnAll(i,j);
          }
      }

    MltAdd(alpha, temp, B, beta, C);
  }


  //! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$ where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] A matrix.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B, times \a
    alpha, plus \a beta times \a C.
  */
  template <class T,
            class Prop1, class Allocator1,
            class Prop2, class Allocator2,
            class Prop4, class Allocator4>
  void MltAdd(const T alpha,
              const Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A,
              const Matrix<T, Prop2, PETScMPIAIJ, Allocator2>& B,
              const T beta,
              Matrix<T, Prop4, PETScMPIAIJ, Allocator4>& C)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    //CheckDim(A, B, C, "MltAdd(alpha, A, B, beta, C)");
#endif


    MatScale(C.GetPetscMatrix(), beta);

    Mat temp;
     MatMatMultSymbolic(A.GetPetscMatrix(),B.GetPetscMatrix(),
                       PETSC_DEFAULT, &temp);

    MatMatMultNumeric(A.GetPetscMatrix(),B.GetPetscMatrix(),
    temp);
        // C = C + alpha * temp

    MatAXPY(C.GetPetscMatrix(), alpha,
            temp, DIFFERENT_NONZERO_PATTERN);
    MatDestroy(&temp);
        //}
  }


  //! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$
    or \f$C = \alpha A B^T + \beta C \f$, where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] TransA status of A: it must be SeldonNoTrans. This argument
    is required for consistency with the interface for full matrices.
    \param[in] A matrix.
    \param[in] TransB status of B: SeldonNoTrans or SeldonTrans.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B or \a
    B^T, times \a alpha, plus \a beta times \a C.
  */
  template <class T,
	    class Prop1, class Allocator1,
	    class Prop2, class Allocator2,
	    class Prop4, class Allocator4>
  void MltAdd(const T alpha,
	      const SeldonTranspose& TransA,
	      const Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A,
	      const SeldonTranspose& TransB,
	      const Matrix<T, Prop2, PETScMPIAIJ, Allocator2>& B,
	      const T beta,
	      Matrix<T, Prop4, PETScMPIAIJ, Allocator4>& C)
  {
    if (!TransA.NoTrans())
      throw WrongArgument("MltAdd(const T0 alpha, SeldonTranspose TransA, "
                          "const Matrix<T1, Prop1, Storage1, Allocator1>& A"
                          "SeldonTranspose TransB,"
                          "const Matrix<T2, Prop2, Storage2, Allocator2>& B,"
                          "const T3 beta,"
                          "Matrix<T4, Prop4, Storage4, Allocator4>& C)",
                          "'TransA' must be equal to 'SeldonNoTrans'.");
    if (!TransB.NoTrans() && !TransB.Trans())
      throw WrongArgument("MltAdd(const T0 alpha, SeldonTranspose TransA, "
                          "const Matrix<T1, Prop1, Storage1, Allocator1>& A"
                          "SeldonTranspose TransB,"
                          "const Matrix<T2, Prop2, Storage2, Allocator2>& B,"
                          "const T3 beta,"
                          "Matrix<T4, Prop4, Storage4, Allocator4>& C)",
                          "'TransB' must be equal to 'SeldonNoTrans' or "
                          "'SeldonTrans'.");

    if (!TransB.Trans())
      MltAdd(alpha, A, B, beta, C);
    else
      {

#ifdef SELDON_CHECK_DIMENSIONS
        CheckDim(SeldonNoTrans, A, SeldonTrans, B, C,
                 "MltAdd(alpha, TransA, A, TransB, B, beta, C)");
#endif
        int m,n;
        Mat* sub_B;
        IS rowIS,colIS;
        ISCreateStride(MPI_COMM_SELF, B.GetM(), 0, 1, &rowIS);
        ISCreateStride(MPI_COMM_SELF, B.GetN(), 0, 1, &colIS);

        MatGetOwnershipRange(A.GetPetscMatrix(),&m,&n);
        MatGetSubMatrices(B.GetPetscMatrix(), 1,
                          const_cast<IS*>(&rowIS),
                          const_cast<IS*>(&colIS),
                          MAT_INITIAL_MATRIX, &sub_B);


        for (unsigned int i = m; i < n; i++)
          {
            int non_zero_A, non_zero_B;
            const int* col_A;
            const int* col_B;
            const double* value_A;
            const double* value_B;
            MatGetRow(A.GetPetscMatrix(), i, &non_zero_A, &col_A, &value_A);
            for (unsigned int j = 0; j < A.GetN(); j++)
              {
                T value = 0;
                MatGetRow(sub_B[0], j, &non_zero_B, &col_B, &value_B);
                int
                  iterator_A = 0,
                  iterator_B = 0;
                while (iterator_A < non_zero_A && iterator_B < non_zero_B)
                  {
                    if (col_A[iterator_A] == col_B[iterator_B])
                      {
                        value += value_A[iterator_A] * value_B[iterator_B];
                        ++iterator_A;
                        ++iterator_B;
                      }
                    else if (col_A[iterator_A] < col_B[iterator_B])
                      ++iterator_A;
                    else
                      ++iterator_B;

                  }//while
                MatRestoreRow(sub_B[0], i, &non_zero_B, &col_B, &value_B);
                value *= alpha;
                if (beta != 0)
                  value += C(i,j) * beta;
                C.SetBuffer(i, j, value);
              } // j
            MatRestoreRow(A.GetPetscMatrix(), i,
                          &non_zero_A, &col_A, &value_A);
          } // boucle i

        C.Flush();
        MatDestroyMatrices(1, &sub_B);
        ISDestroy(&rowIS);
        ISDestroy(&colIS);
      }
  }


 //! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$
    or \f$C = \alpha A B^T + \beta C \f$, where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] TransA status of A: it must be SeldonNoTrans. This argument
    is required for consistency with the interface for full matrices.
    \param[in] A matrix.
    \param[in] TransB status of B: SeldonNoTrans or SeldonTrans.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B or \a
    B^T, times \a alpha, plus \a beta times \a C.
  */
  template <class T,
	    class Prop1, class Allocator1,
	    class Prop2, class Storage2, class Allocator2,
	    class Prop4, class Storage4, class Allocator4>
  void MltAdd(const T alpha,
	      const SeldonTranspose& TransA,
	      const Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A,
	      const SeldonTranspose& TransB,
	      const Matrix<T, Prop2, Storage2, Allocator2>& B,
	      const T beta,
	      Matrix<T, Prop4, Storage4, Allocator4>& C)
  {
    if (!TransA.NoTrans())
      throw WrongArgument("MltAdd(const T0 alpha, SeldonTranspose TransA, "
                          "const Matrix<T1, Prop1, Storage1, Allocator1>& A"
                          "SeldonTranspose TransB,"
                          "const Matrix<T2, Prop2, Storage2, Allocator2>& B,"
                          "const T3 beta,"
                          "Matrix<T4, Prop4, Storage4, Allocator4>& C)",
                          "'TransA' must be equal to 'SeldonNoTrans'.");
    if (!TransB.NoTrans() && !TransB.Trans())
      throw WrongArgument("MltAdd(const T0 alpha, SeldonTranspose TransA, "
                          "const Matrix<T1, Prop1, Storage1, Allocator1>& A"
                          "SeldonTranspose TransB,"
                          "const Matrix<T2, Prop2, Storage2, Allocator2>& B,"
                          "const T3 beta,"
                          "Matrix<T4, Prop4, Storage4, Allocator4>& C)",
                          "'TransB' must be equal to 'SeldonNoTrans' or "
                          "'SeldonTrans'.");

    if (!TransB.Trans())
      MltAdd(alpha, A, B, beta, C);
    else
      {

#ifdef SELDON_CHECK_DIMENSIONS
        CheckDim(SeldonNoTrans, A, SeldonTrans, B, C,
                 "MltAdd(alpha, TransA, A, TransB, B, beta, C)");
#endif


        int B_m = B.GetM();
        int B_n = B.GetN();

        Matrix<T, Prop4, Storage4, Allocator4> temp(B_m, B_n);
        for (unsigned int i = 0; i < B_m; i++)
          {
            for (unsigned int j =  0 ; j < B_n; j++)
              {
                temp(i,j) = A.GetOnAll(i,j);
              }
          }
        MltAdd(alpha, TransA, temp, TransB, B, beta, C);
      }
  }




//! Multiplies two matrices, and adds the result to a third matrix.
  /*! It performs the operation \f$ C = \alpha A B + \beta C \f$
    or \f$C = \alpha A B^T + \beta C \f$, where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, and \f$ A \f$, \f$ B \f$ and \f$
    C \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] TransA status of A: it must be SeldonNoTrans. This argument
    is required for consistency with the interface for full matrices.
    \param[in] A matrix.
    \param[in] TransB status of B: SeldonNoTrans or SeldonTrans.
    \param[in] B matrix.
    \param[in] beta scalar.
    \param[in,out] C matrix, result of the product of \a A with \a B or \a
    B^T, times \a alpha, plus \a beta times \a C.
  */
  template <class T,
	    class Prop1, class Storage1, class Allocator1,
	    class Prop2, class Storage2, class Allocator2,
	    class Prop4, class Allocator4>
  void MltAdd(const T alpha,
	      const SeldonTranspose& TransA,
	      const Matrix<T, Prop1, Storage1, Allocator1>& A,
	      const SeldonTranspose& TransB,
	      const Matrix<T, Prop2, Storage2, Allocator2>& B,
	      const T beta,
	      Matrix<T, Prop4, PETScMPIAIJ, Allocator4>& C)
  {
    Matrix<T> temp;
    int m = A.GetM();
    int n = A.GetN();
    temp.Reallocate(m, n);
    for(int i = 0; i < m; i++)
      {
        for (int j = 0; j < n; j++)
          {
            temp(i, j) = C.GetOnAll(i,j);
          }
      }
    MltAdd(alpha, TransA, A, TransB, B, beta, temp);
    for(int i = 0; i < m; i++)
      {
        for (int j = 0; j < n; j++)
          {
            C.SetBuffer(i, j, temp(i, j));
          }
      }
    C.Flush();
  }


  // MLTADD //
  ////////////


  /////////
  // ADD //


  //! Adds two matrices.
  /*! It performs the operation \f$ B = \alpha A + B \f$ where \f$ \alpha \f$
    is a scalar, and \f$ A \f$ and \f$ B \f$ are matrices.
    \param[in] alpha scalar.
    \param[in] A matrix.
    \param[in,out] B matrix, result of the addition of \a B (on entry) and \a
    A times \a alpha.
  */
  template<class T, class Prop1, class Allocator1,
	   class Prop2, class Allocator2>
  void Add(const T& alpha,
	   const Matrix<T, Prop1, PETScMPIAIJ, Allocator1>& A,
	   Matrix<T, Prop2, PETScMPIAIJ, Allocator2>& B)
  {
    MatAXPY(B.GetPetscMatrix(), alpha,
            A.GetPetscMatrix(), DIFFERENT_NONZERO_PATTERN);
  }


  // ADD //
  /////////


  ///////////////
  // TRANSPOSE //


  //! Matrix transposition.
  template<class T, class Prop, class Allocator>
  void Transpose(Matrix<T, Prop, PETScMPIAIJ, Allocator>& A)
  {
    MatTranspose(A.GetPetscMatrix(), MAT_REUSE_MATRIX, &(A.GetPetscMatrix()));
  }


  // TRANSPOSE //
  ///////////////


} // namespace Seldon.


#endif
