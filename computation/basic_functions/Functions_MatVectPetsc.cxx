#ifndef SELDON_FILE_FUNCTIONS_MATVECT_PETSC_CXX
#define SELDON_FILE_FUNCTIONS_MATVECT_PETSC_CXX


namespace Seldon
{


  ////////////
  // MLTADD //


  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
              const Vector<T2, PETScSeq, Allocator2>& X,
              const T3 beta, Vector<T4, PETScSeq, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif
    if (beta == T3(0))
      if (alpha == T0(0))
        {
          Y.Fill(T4(0));
          return;
        }
      else
        {
          MatMult(M.GetPetscMatrix(), X.GetPetscVector(), Y.GetPetscVector());
          if (alpha != T0(1))
            VecScale(Y.GetPetscVector(), alpha);
          return;
        }
    if (alpha == T0(1))
      {
        if (beta != T3(1))
          VecScale(Y.GetPetscVector(), beta);
        MatMultAdd(M.GetPetscMatrix(), X.GetPetscVector(),
                   Y.GetPetscVector(), Y.GetPetscVector());
        return;
      }
    Vector<T2, PETScSeq, Allocator2> tmp;
    tmp.Copy(Y);
    MatMult(M.GetPetscMatrix(), X.GetPetscVector(), tmp.GetPetscVector());
    VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
    return;
  }

  /*! \brief Performs the multiplication of a matrix with a vector, and adds
    the result to another vector.
   */
  /*! It performs the operation \f$ Y = \alpha M X + \beta Y \f$ where \f$
    \alpha \f$ and \f$ \beta \f$ are scalars, \f$ M \f$ is a \f$ m \times n
    \f$ matrix, and \f$ X \f$ is a vector of length \f$ n \f$. The vector \f$
    Y \f$ must be of length \f$ m \f$.
    \param[in] alpha scalar.
    \param[in] M m by n matrix.
    \param[in] X vector of length n.
    \param[in] beta scalar.
    \param[in,out] Y vector of length m, result of the product of \a M by \a
    X, times \a alpha, plus \a Y (on entry) times \a beta.
  */
  template <class T0,
	    class T1, class Prop1, class Storage1, class Allocator1,
	    class T2, class Storage2, class Allocator2,
	    class T3,
	    class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
	      const Matrix<T1, Prop1, Storage1, Allocator1>& M,
	      const Vector<T2, Storage2, Allocator2>& X,
	      const T3 beta,
	      Vector<T4, PETScPar, Allocator4>& Y)
  {
    int ma = M.GetM();
    int na = M.GetN();

#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif

    Mlt(beta, Y);
    int l,h;
    VecGetOwnershipRange(Y.GetPetscVector(), &l, &h);
    T4 zero(0);
    T4 temp;
    T4 alpha_(alpha);
    for (int i = l; i < h; i++)
      {
	temp = zero;
	for (int j = 0; j < na; j++)
	  temp += M(i, j) * X(j);
        Y.SetBuffer(i, temp * alpha_ + Y(i));
      }
    Y.Flush();
  }

  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
              const Vector<T2, PETScPar, Allocator2>& X,
              const T3 beta, Vector<T4, PETScPar, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif
    if (beta == T3(0))
      if (alpha == T0(0))
        {
          Y.Fill(T4(0));
          return;
        }
      else
        {
          MatMult(M.GetPetscMatrix(), X.GetPetscVector(), Y.GetPetscVector());
          if (alpha != T0(1))
            VecScale(Y.GetPetscVector(), alpha);
          return;
        }
    if (alpha == T0(1))
      {
        if (beta != T3(1))
          VecScale(Y.GetPetscVector(), beta);
        MatMultAdd(M.GetPetscMatrix(), X.GetPetscVector(),
                   Y.GetPetscVector(),Y.GetPetscVector());
        return;
      }
    Vector<T2, PETScPar, Allocator2> tmp;
    tmp.Copy(Y);
    MatMult(M.GetPetscMatrix(), X.GetPetscVector(), tmp.GetPetscVector());
    VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
    return;
  }


  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
              const Vector<T2, VectFull, Allocator2>& X,
              const T3 beta, Vector<T4, PETScSeq, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif

    Vector<T4, PETScSeq, Allocator4> X_Petsc;
    X_Petsc.Reallocate(X.GetM());
    for (int i = 0; i < X.GetM(); i++)
      X_Petsc.SetBuffer(i, X(i));
    X_Petsc.Flush();

    if (beta == T3(0))
      if (alpha == T0(0))
        {
          Y.Fill(T4(0));
          return;
        }
      else
        {
          MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                  Y.GetPetscVector());
          if (alpha != T0(1))
            VecScale(Y.GetPetscVector(), alpha);
          return;
        }
    if (alpha == T0(1))
      {
        if (beta != T3(1))
          VecScale(Y.GetPetscVector(), beta);
        MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                   Y.GetPetscVector(),Y.GetPetscVector());
        return;
      }
    Vector<T2, PETScSeq, Allocator2> tmp;
    tmp.Copy(Y);
    MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
            tmp.GetPetscVector());
    VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
    return;
  }


  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T0, Prop1, PETScMPIAIJ, Allocator1>& M,
              const Vector<T0, VectFull, Allocator2>& X,
              const T0 beta, Vector<T4, PETScPar, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif

    Vector<T4, PETScPar, Allocator4> X_Petsc;
    X_Petsc.SetCommunicator(M.GetCommunicator());
    X_Petsc.Reallocate(X.GetM());
    for (int i = 0; i < X.GetM(); i++)
      X_Petsc.SetBuffer(i, X(i));
    X_Petsc.Flush();

    if (beta == T3(0))
      if (alpha == T0(0))
        {
          Y.Fill(T4(0));
          return;
        }
      else
        {
          MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                  Y.GetPetscVector());
          if (alpha != T0(1))
            VecScale(Y.GetPetscVector(), alpha);
          return;
        }
    if (alpha == T0(1))
      {
        if (beta != T3(1))
          VecScale(Y.GetPetscVector(), beta);
        MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                   Y.GetPetscVector(),Y.GetPetscVector());
        return;
      }
    Vector<T2, PETScPar, Allocator2> tmp;
    tmp.Copy(Y);
    MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
            tmp.GetPetscVector());
    VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
    return;
  }


  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIDense, Allocator1>& M,
              const Vector<T2, VectFull, Allocator2>& X,
              const T3 beta, Vector<T4, PETScPar, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif

    Vector<T4, PETScPar, Allocator4> X_Petsc;
     X_Petsc.SetCommunicator(M.GetCommunicator());
    X_Petsc.Reallocate(X.GetM());
    for (int i = 0; i < X.GetM(); i++)
      X_Petsc.SetBuffer(i, X(i));
    X_Petsc.Flush();

    if (beta == T3(0))
      if (alpha == T0(0))
        {
          Y.Fill(T4(0));
          return;
        }
      else
        {
          MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                  Y.GetPetscVector());
          if (alpha != T0(1))
            VecScale(Y.GetPetscVector(), alpha);
          return;
        }
    if (alpha == T0(1))
      {
        if (beta != T3(1))
          VecScale(Y.GetPetscVector(), beta);
        MatMultAdd(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
                   Y.GetPetscVector(),Y.GetPetscVector());
        return;
      }
    Vector<T2, PETScPar, Allocator2> tmp;
    tmp.Copy(Y);
    tmp.SetCommunicator(M.GetCommunicator());
    MatMult(M.GetPetscMatrix(), X_Petsc.GetPetscVector(),
            tmp.GetPetscVector());
    VecAXPBY(Y.GetPetscVector(), alpha, beta, tmp.GetPetscVector());
    return;
  }


  template <class T0,
            class T1, class Prop1, class Allocator1,
            class T2, class Allocator2,
            class T3,
            class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
              const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
              const Vector<T2, PETScPar, Allocator2>& X,
              const T3 beta, Vector<T4, VectFull, Allocator4>& Y)
  {
#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(M, X, Y, "MltAdd(alpha, M, X, beta, Y)");
#endif

    Vector<T4, PETScPar, Allocator4> Y_Petsc;
     Y_Petsc.SetCommunicator(M.GetCommunicator());
    Y_Petsc.Reallocate(Y.GetM());
    for (int i = 0; i < Y.GetM(); i++)
      Y_Petsc.SetBuffer(i, Y(i));
    Y_Petsc.Flush();
    MltAddVector(alpha, M, X, beta, Y_Petsc);
    for (int i = 0; i < Y.GetM(); i++)
      Y(i) = Y_Petsc(i);
    return;
  }


  /*! \brief Performs the multiplication of a matrix (possibly transposed)
    with a vector, and adds the result to another vector.
   */
  /*! It performs the operation \f$ Y = \alpha M X + \beta Y \f$ or \f$ Y =
    \alpha M^T X + \beta Y \f$ where \f$ \alpha \f$ and \f$ \beta \f$ are
    scalars, \f$ M \f$ is a \f$ m \times n \f$ matrix or a \f$ n \times m \f$
    matrix, and \f$ X \f$ is a vector of length \f$ n \f$. The vector \f$ Y
    \f$ must be of length \f$ m \f$.
    \param[in] alpha scalar.
    \param[in] Trans transposition status of \a M: it may be SeldonNoTrans or
    SeldonTrans.
    \param[in] M m by n matrix, or n by m matrix if transposed.
    \param[in] X vector of length n.
    \param[in] beta scalar.
    \param[in,out] Y vector of length m, result of the product of \a M by \a
    X, times \a alpha, plus \a Y (on entry) times \a beta.
    \note \a Trans must be either SeldonNoTrans or SeldonTrans: ConjTrans is
    not supported.
  */
  template <class T0,
	    class T1, class Prop1, class Allocator1,
	    class T2, class Allocator2,
	    class T3,
	    class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
	      const SeldonTranspose& Trans,
	      const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
	      const Vector<T2, PETScPar, Allocator2>& X,
	      const T3 beta,
	      Vector<T4, PETScPar, Allocator4>& Y)
  {
    if (Trans.NoTrans())
      {
        MltAdd(alpha, M, X, beta, Y);
        return;
      }
    else if (Trans.ConjTrans())
      throw WrongArgument("MltAdd(alpha, trans, M, X, beta, Y)",
                          "Complex conjugation not supported.");

#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(Trans, M, X, Y, "MltAdd(alpha, trans, M, X, beta, Y)");
#endif
    Matrix<T1, Prop1, PETScMPIAIJ, Allocator1> temp;
    temp.Copy(M);
    Transpose(temp);
    MltAdd(alpha, temp, X, beta, Y);
  }

  /*! \brief Performs the multiplication of a matrix (possibly transposed)
    with a vector, and adds the result to another vector.
   */
  /*! It performs the operation \f$ Y = \alpha M X + \beta Y \f$ or \f$ Y =
    \alpha M^T X + \beta Y \f$ where \f$ \alpha \f$ and \f$ \beta \f$ are
    scalars, \f$ M \f$ is a \f$ m \times n \f$ matrix or a \f$ n \times m \f$
    matrix, and \f$ X \f$ is a vector of length \f$ n \f$. The vector \f$ Y
    \f$ must be of length \f$ m \f$.
    \param[in] alpha scalar.
    \param[in] Trans transposition status of \a M: it may be SeldonNoTrans or
    SeldonTrans.
    \param[in] M m by n matrix, or n by m matrix if transposed.
    \param[in] X vector of length n.
    \param[in] beta scalar.
    \param[in,out] Y vector of length m, result of the product of \a M by \a
    X, times \a alpha, plus \a Y (on entry) times \a beta.
    \note \a Trans must be either SeldonNoTrans or SeldonTrans: ConjTrans is
    not supported.
  */
  template <class T0,
	    class T1, class Prop1, class Allocator1,
	    class T2, class Allocator2,
	    class T3,
	    class T4, class Allocator4>
  void MltAddVector(const T0 alpha,
	      const SeldonTranspose& Trans,
	      const Matrix<T1, Prop1, PETScMPIAIJ, Allocator1>& M,
	      const Vector<T2, PETScPar, Allocator2>& X,
	      const T3 beta,
	      Vector<T4, VectFull, Allocator4>& Y)
  {
    if (Trans.NoTrans())
      {
        MltAdd(alpha, M, X, beta, Y);
        return;
      }
    else if (Trans.ConjTrans())
      throw WrongArgument("MltAdd(alpha, trans, M, X, beta, Y)",
                          "Complex conjugation not supported.");

#ifdef SELDON_CHECK_DIMENSIONS
    CheckDim(Trans, M, X, Y, "MltAdd(alpha, trans, M, X, beta, Y)");
#endif
    Matrix<T1, Prop1, PETScMPIAIJ, Allocator1> temp;
    temp.Copy(M);
    Transpose(temp);
    MltAdd(alpha, temp, X, beta, Y);
  }


  // MLTADD //
  ////////////

}

#endif
